﻿# Arbre Rouge Noir


Il s'agit d'un TP réalisé en binôme pour l'UE de L3 Informatique "Algorithmique, Programmation et Complexité". 
Le but était d'implanter une collection d'éléments sous forme d'Arbre Binaire de Recherche et d'Arbre Rouge Noir, afin de comparer les performances de ces deux types abstraits.


# Auteurs

@**BOUDRAA Abdelhakim**  11805901
**mail** : abdelhakim.boudraa@etu.univ-lyon1.fr

----
@**RAMPON Ildès**  89064800
**mail** : ildes.rampon@etu.univ-lyon1.fr

----

# Compiler le projet avec le makefile

Commande make


# Exécutable

bin/collection


# Différents fichiers dans src/

|Fichier|Brève description|Principalesfonctionnalités|
|--|--|--|
|collectionABR.h et .cpp|Classe ABR, Arbre Binaire de Recherche|Constructeur, destructeur, insertion, recherche, affichage|
|collectionARN.h et .cpp|Classe ARN, Arbre Rouge Noir|Constructeur, destructeur, insertion, recherche, affichage|
|noeud.h|Structure noeud|Utile pour définir un ABR et un ARN|
|element.h et .cpp|Définition du type Element|Affichage|
|main.cpp|Propose l'affichage d'un ARN et crée des fichiers pour créer des courbes de temps avec gnuplot


# Fichiers générés

main.cpp crée 4 fichiers, formés d'une colonne avec des tailles d'arbre et une colonne avec des temps (d'insertion ou de recherche d'un élément): 
	performancesABRInsertion.txt
	performancesABRRecherche.txt
	performancesARNInsertion.txt
	performancesARNRecherche.txt


# Courbes avec gnuplot

Pour visualiser les temps d'insertion et de recherche d'un élément en fonction de la taille de l'arbre, temps enregistrés dans les 4 fichiers précédents:

	taper 
	- gnuplot
	puis
	- plot "performancesABRInsertion.txt" w l,"performancesARNInsertion.txt" w l 
	ou
	- plot "performancesABRRecherche.txt" w l,"performancesARNRecherche.txt" w l


Pour changer l'échelle de l'axe des ordonnées:  set yrange [0:2] par exemple 

