#include "collectionARN.h"
#include <iostream>
#include <string>

using namespace std;

 
ARN::ARN()
{
    adracine = nullptr;
    nbElement = 0;
}


ARN::ARN(const ARN & a)
{
    adracine = nullptr;
    *this = a;
}


ARN::~ARN()
{
    vider();
    adracine = nullptr;
}


void ARN::rechercherDansSousArbre (const Element & e,const Noeud * n, bool & estPresent) const
{
    if (n != nullptr)
    {
        if (n->info == e)
            estPresent = true;
        else
        {
            if (e < n->info)
                rechercherDansSousArbre (e,n->fg,estPresent);
            else
                rechercherDansSousArbre (e,n->fd,estPresent);
        }
    }
}


bool ARN::rechercherElement (const Element & e) const
{
    bool estPresent = false;
    rechercherDansSousArbre(e,adracine,estPresent);
    return estPresent;
}


void ARN::afficherParcoursInfixeSousArbre (Noeud * n, int nbEspaces) const
{
    if (n != nullptr)
    {
        afficherParcoursInfixeSousArbre (n->fd, nbEspaces+10);
        cout << endl;
        for (int i = 0; i<nbEspaces; i++)
            cout << " ";
        if (n->couleur == "rouge")
            cout << rouge;
        else
            cout << bleu;
        affichageElement(n->info);
        cout << reset << endl;
        afficherParcoursInfixeSousArbre (n->fg, nbEspaces+10);
    }

}


void ARN::afficherParcoursInfixe () const
{
    afficherParcoursInfixeSousArbre(adracine,0);
}


void ARN::copieProfondeSousArbre (Noeud * n)
{
    if (n != nullptr)
    {
        insererElement(n->info);
        copieProfondeSousArbre(n->fg);
        copieProfondeSousArbre(n->fd);
    }
}


void ARN::operator = (const ARN & a)
{
    copieProfondeSousArbre(a.adracine);
}


void ARN::viderSousArbre (Noeud * n)
{
    if (n != nullptr)
    {
        viderSousArbre (n->fg);
        
        viderSousArbre (n->fd);
        delete n;
        nbElement--;
    }
}


void ARN::vider ()
{
    viderSousArbre(adracine);
}


Noeud* ARN::ajouterFeuille (const Element & e, Noeud * nP)
{
    Noeud * n = new Noeud;
    n->couleur = "rouge";
    n->info = e;
    n->fg = nullptr;
    n->fd = nullptr;
    nbElement++;
    return n;
}


void ARN::insererDansSousArbre (const Element &e, Noeud * & n)
{
    if (e < n->info)
    {
        if (n->fg == nullptr)
            n->fg = ajouterFeuille(e,n);

        else    // on descend dans le sous-arbre
        {
            insererDansSousArbre(e,n->fg);
                
            
            if (nbElement>2 && n->fg->couleur == "rouge")   // si nbElement est égal à 1 ou 2, il n'y a que la couleur de la racine à changer, dans insérerElement
                changerSousArbre(n,n->fg);  //grand-père et père dans changerSousArbre
        }
    }
    else
    {
        if (n->fd == nullptr)
            n->fd = ajouterFeuille(e,n);

        else    // on descend dans le sous-arbre
        {
            insererDansSousArbre(e,n->fd);
            
            // on remonte dans le sous-arbre
            if (nbElement>2 && n->fd->couleur == "rouge")   // si nbElement est égal à 1 ou 2, il n'y a que la couleur de la racine à changer, dans insérerElement
                                                            // si n->fd, le père, est noir, il n'y a pas de problème
                changerSousArbre(n,n->fd);
        }
    }
}


void ARN::insererElement (const Element & e)
{
    if (nbElement == 0) // l'arbre est vide
    {
        adracine = ajouterFeuille (e,adracine);
        adracine->couleur = "noir";
    }
    else    //l'arbre n'est pas vide
        insererDansSousArbre(e,adracine);
    
    
}


void ARN::changerCouleur (Noeud * n)
{
    if (n->couleur == "noir")
    {
        n->couleur = "rouge";
    }
    else
    {
        n->couleur = "noir";
    }
}


void ARN::changerSousArbre (Noeud* &PP, Noeud* &P)  // PP est le noeud grand-père, P est le noeud père 
{

    char lienFils_Pere, lienPere_GrandPere; // liens utiles pour déterminer quelle rotation faire

    if (P->fg != nullptr && P->fg->couleur == "rouge")  // le fils qui nous intéresse est celui qui fait qu'il y a un problème, c'est-à-dire celui qui est rouge
        {lienFils_Pere = 'g';}
    else if (P->fd != nullptr && P->fd->couleur == "rouge")
        {lienFils_Pere = 'd';}
    

    if (PP->fg == P)   
        {lienPere_GrandPere = 'g';}
    else              
        {lienPere_GrandPere = 'd';}

   

    if (PP->fg != nullptr && PP->fd != nullptr && (PP->fg)->couleur == "rouge" && (PP->fd)->couleur == "rouge") /* cas 1: on est sûr que l'oncle existe, à gauche ou à droite, et le père et l'oncle sont rouges comme le fils */
    {
        changerCouleur(PP);
        changerCouleur(PP->fg);
        changerCouleur(PP->fd);
        if (PP == adracine) PP->couleur = "noir";
    }
    else
    {
        if (lienFils_Pere == 'g' && lienPere_GrandPere == 'g') /* cas 2a: le fils gauche de P est le fils gauche de PP */
            {   
                changerCouleur(PP->fg);
                changerCouleur(PP);  
                rotationSimpleDroite(PP);   
            }
        if (lienFils_Pere == 'd' && lienPere_GrandPere == 'g') /* cas 2b: le fils droit de P est le fils gauche de PP */
            {rotationDoubleDroite(PP);}
        if (lienFils_Pere == 'd' && lienPere_GrandPere == 'd') /* cas 2c: le fils droit de P est le fils droit de PP */
            {   changerCouleur(PP->fd);
                changerCouleur(PP);   
                rotationSimpleGauche(PP);
            }
        if (lienFils_Pere == 'g' && lienPere_GrandPere == 'd') /* cas 2d: le fils gauche de P est le fils droit de PP */
            {rotationDoubleGauche(PP);}
    }
   
}


void ARN::rotationSimpleDroite(Noeud* &PP)
{
    Noeud * tmp = new Noeud;
    
    tmp = PP ->fg;      
    PP->fg = PP->fg->fd;
    tmp->fd = PP;
    PP=tmp;

    if (PP == adracine) PP->couleur = "noir";
}


void ARN::rotationSimpleGauche(Noeud* &PP)
{
 

    Noeud * tmp = new Noeud;

    tmp = PP->fd;
    PP->fd = PP->fd->fg;
    tmp->fg = PP;
    PP=tmp;

    if (PP == adracine) PP->couleur = "noir";
}


void ARN::rotationDoubleDroite(Noeud* &PP)
{
    rotationSimpleGauche(PP->fg);
    changerCouleur(PP->fg);
    changerCouleur(PP);
    rotationSimpleDroite(PP);
}


void ARN::rotationDoubleGauche(Noeud* &PP)
{
    rotationSimpleDroite(PP->fd);
    changerCouleur(PP->fd);
    changerCouleur(PP);
    rotationSimpleGauche(PP);
}