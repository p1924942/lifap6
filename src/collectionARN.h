#ifndef _ARN_H
#define _ARN_H

#include <string>
#include "element.h"
#include "noeud.h"

using namespace std;


/* Pour l'affichage des noeuds en couleur */
const string rouge("\033[0;31m");
const string bleu("\033[0;34m");
const string reset("\033[0m");	/* remise à la couleur par défaut */



class ARN {

private:

    Noeud * adracine;
	int nbElement;

	
	/* 	Postcondition : Recherche dans le sous arbre de racine n.
                     	Si on a trouvé un noeud avec le e demand�, alors estPresent prend la valeur vrai. */
	void rechercherDansSousArbre (const Element & e,const Noeud * n, bool & estPresent) const;


	/* 	Postcondition : Affichage infixe de tous les éléments de l'arbre (espacement entre les noeuds). */
	void afficherParcoursInfixeSousArbre (Noeud * n, int nbEspaces) const;


	/* 	Postcondition : Copie chaque noeud du sous-arbre de racine n dans this. */
	void copieProfondeSousArbre (Noeud * n);


	/* 	Postcondition : Le sous arbre de racine n est vidé */
	void viderSousArbre (Noeud * n);


	/* 	Précondition : Le sous-arbre n'est pas vide.
		Postcondition : Insère l'élément e dans le sous arbre de racine n.
						Si e n'existe pas déjà dans l'arbre, alors un nouveau noeud contenant e est inséré. */
	void insererDansSousArbre (const Element & e, Noeud * & n);


	/* 	Postcondition : Retourne un pointeur sur Noeud dont le père est nP et l'info est e. */
	Noeud* ajouterFeuille (const Element & e, Noeud * nP);


	/* 	Postcondition : n->couleur est changée. */
	void changerCouleur (Noeud * n);


	/* 	Précondition : nbElement>2 càd fils, père et grand-père ne sont pas nullptr.
		Postcondition : Teste s'il y a un problème de couleur, c'est-à-dire P et un de ses fils sont rouges.
						Fait les modifications nécessaires selon la configuration parmi 5. */
	void changerSousArbre (Noeud* &PP,Noeud* &P);

	/* 	Précondition : 	Cas 2a, PP est le noeud grand-père.
		Postcondition : Effectue les changements correspondant à la rotation simple droite du sous-arbre de racine PP. */
	void rotationSimpleDroite(Noeud* &PP);

	/* 	Précondition : 	Cas 2c, PP est le noeud grand-père.
		Postcondition : 	Effectue les changements correspondant à la rotation simple gauche du sous-arbre de racine PP. */
	void rotationSimpleGauche(Noeud* &PP);

	/* 	Précondition : 	Cas 2b, PP est le noeud grand-père.
		Postcondition : Effectue les changements correspondant à la rotation double droite du sous-arbre de racine PP. */
	void rotationDoubleDroite(Noeud* &PP);

	/* 	Précondition : 	Cas 2d, PP est le noeud grand-père.
		Postcondition : Effectue les changements correspondant à la rotation double gauche du sous-arbre de racine PP. */
	void rotationDoubleGauche(Noeud* &PP);


public:

	/*	Postcondition : l'arbre est créé vide */
    ARN ();


	/*	Postcondition : l'arbre est créé avec les valeurs de a */
    ARN (const ARN & a);
    

	/*	Postcondition : l'arbre est d'abord vidé, puis adracine est mis à nullptr */
    ~ARN ();


	/*	Postcondition : L'arbre est vide */
	void vider ();
    

	/*	Postcondition : On affecte chaque noeud de a à this. */
	void operator = (const ARN & a);
	

	/*	Postcondition : Si aucun noeud de l'arbre ne contient la valeur e, retourne faux.
                       Si on a trouvé un noeud avec le e demandé, alors retourne vrai.*/
	bool rechercherElement (const Element & e) const;
   

	/*	Postcondition : Un nouveau noeud contenant e est inséré dans l'arbre. */
	void insererElement (const Element & e);
    

	/*	Postcondition : affichage infixe de tous les éléments de l'arbre. */
	void afficherParcoursInfixe () const;

	

};

#endif
