#include <cstdio>
#include <iostream>
#include <fstream> //ofstream
#include <chrono>
#include <stdlib.h> //rand
#include "element.h" //offrant le type Element
#include "collectionARN.h"
#include "collectionABR.h"

using namespace std;

const int NB_ARBRES = 200;
const int TAILLE_ARBRE = 10000;




void creeFichierAvecTableau(const char * nomFichier, const float * tab, int taille)
//preconditions : nomFichier chaine de caracteres designant le nom du fichier a creer
//postcondition : le fichier nomFichier contient taille lignes composées d'un nombre de données et d'un temps (contenu dans tab) separes par un espace
{
  ofstream ofs;
  ofs.open(nomFichier);
  if(ofs.bad()) 
    {cout<<"Impossible d'ouvrir le fichier "<<nomFichier<<" en ecriture \n"; exit(1);}

  for(int i=0;i<TAILLE_ARBRE/20;i++)
    {
      ofs << (i+1)*20 << ' ' << tab[i] << endl;
    }
  ofs.close();
}

template <typename arbre>
void creationTabTempsInsertionRecherche (arbre tabArbres[TAILLE_ARBRE] , float tabTempsInsertion[TAILLE_ARBRE], float tabTempsRecherche[TAILLE_ARBRE])
{
    chrono::time_point<chrono::system_clock> debut, fin_insertion, debut_recherche, fin_recherche;
    float duree_microseconds;
    int nbCherche;

    for(int i=0;i<TAILLE_ARBRE;i++) // on augmente la taille des arbres jusqu'à la taille voulue
    {
        debut = chrono::system_clock::now();
        for(int j=0; j<NB_ARBRES; j++)  // insertion d'un élément dans les NB_ARBRES arbres du tableau
        {
            tabArbres[j].insererElement(rand()%1000);
        }
        fin_insertion = chrono::system_clock::now();
        duree_microseconds = chrono::duration_cast<chrono::microseconds>(fin_insertion-debut).count();
        tabTempsInsertion[i] = duree_microseconds/NB_ARBRES;  // temps moyen d'insertion pour un arbre de taille i+1
        
        
        nbCherche = rand()%1000;
        debut_recherche = chrono::system_clock::now();
        for(int j=0; j<NB_ARBRES; j++)  // recherche d'un élément dans les NB_ARBRES arbres du tableau
        {
            tabArbres[j].rechercherElement(nbCherche);
        }
        fin_recherche = chrono::system_clock::now();
        duree_microseconds = chrono::duration_cast<chrono::microseconds>(fin_recherche-debut_recherche).count();
        tabTempsRecherche[i] = duree_microseconds/NB_ARBRES;   // temps moyen de recherche pour un arbre de taille i+1
        
    }
}



int main()
{
    srand(time(NULL));
    char affichage;
    cout << "Tester l'affichage d'un ARN? O/N" << endl;
    cin >> affichage;
    if (affichage == 'O')
    {
        ARN testAffichage;
        int element;
        do 
        {
            cout << "ajouter un nombre entier positif dans l'arbre:(-1 pour arrêter) " << endl;
            cin >> element;
            if (element >= 0)
                testAffichage.insererElement(element);
        }
        while (element >= 0);
        testAffichage.afficherParcoursInfixe();
        int recherche;
        do
        {
            cout << "Chercher un nombre:(-1 pour arrêter) " << endl;
            cin >> recherche;
            if (recherche >= 0)
            {
                if (testAffichage.rechercherElement(recherche))
                    cout << recherche << " est dans l'arbre." << endl;
                else
                    cout << recherche << " n'est pas dans l'arbre." << endl;
            }
        } while (recherche >= 0);
        

    }


    int i;
    ABR tabABR[NB_ARBRES];
    ARN tabARN[NB_ARBRES];
    for (i = 0; i<NB_ARBRES; i++) // initialisation des tableaux d'ABR et ARN
    {
        ABR* aABR = new ABR();
        tabABR[i] = *aABR;
        ARN* aARN = new ARN();
        tabARN[i] = *aARN;
    }

    // création des tableaux qui vont stocker les temps
    float tabTempsInsertionABR[TAILLE_ARBRE];
    float tabTempsRechercheABR[TAILLE_ARBRE];
    float tabTempsInsertionARN[TAILLE_ARBRE];
    float tabTempsRechercheARN[TAILLE_ARBRE];

    for (i = 0; i < TAILLE_ARBRE; i++)
    {
        tabTempsInsertionABR[i] = 0;
        tabTempsRechercheABR[i] = 0;
        tabTempsInsertionARN[i] = 0;
        tabTempsRechercheARN[i] = 0;
    }

    creationTabTempsInsertionRecherche<ABR>(tabABR,tabTempsInsertionABR,tabTempsRechercheABR);
    creationTabTempsInsertionRecherche<ARN>(tabARN,tabTempsInsertionARN,tabTempsRechercheARN);

    creeFichierAvecTableau("performancesABRInsertion.txt",tabTempsInsertionABR,TAILLE_ARBRE);
    creeFichierAvecTableau("performancesABRRecherche.txt",tabTempsRechercheABR,TAILLE_ARBRE);
    
    creeFichierAvecTableau("performancesARNInsertion.txt",tabTempsInsertionARN,TAILLE_ARBRE);
    creeFichierAvecTableau("performancesARNRecherche.txt",tabTempsRechercheARN,TAILLE_ARBRE);
   
    return 0;
}


