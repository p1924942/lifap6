#include "collectionABR.h"
#include <iostream>

using namespace std;


ABR::ABR()
{
    adracine = nullptr;
    nbElement = 0;
}

ABR::ABR(const ABR & a)
{
    adracine = nullptr;
    *this = a;
}

ABR::~ABR()
{
    vider();
    adracine = nullptr;
}

void ABR::insererDansSousArbre (const Element &e, Noeud * & n)
{
    if (n != nullptr)
    {
        if (e < n->info)
            insererDansSousArbre(e,n->fg);
        else
            insererDansSousArbre(e,n->fd);}
    else
    {
        n = new Noeud;
        n->info = e;
        n->fg = nullptr;
        n->fd = nullptr;
    }   
}

void ABR::insererElement (const Element & e)
{
    insererDansSousArbre(e,adracine);
    nbElement++;
}

void ABR::rechercherDansSousArbre (const Element & e,const Noeud * n, bool & estPresent) const
{
    if (n != nullptr)
    {
        if (n->info == e)
            estPresent = true;
        else
        {
            if (e < n->info)
                rechercherDansSousArbre (e,n->fg,estPresent);
            else
                rechercherDansSousArbre (e,n->fd,estPresent);
        }
    }
}

bool ABR::rechercherElement (const Element & e) const
{
    bool estPresent = false;
    rechercherDansSousArbre(e,adracine,estPresent);
    return estPresent;
}

void ABR::afficherParcoursInfixeSousArbre (Noeud * n, int nbEspaces) const
{
    if (n != nullptr)
    {
        afficherParcoursInfixeSousArbre (n->fd, nbEspaces+10);
        cout << endl;
        for (int i = 0; i<nbEspaces; i++)
            cout << " ";
        affichageElement(n->info);
        cout << endl;
        afficherParcoursInfixeSousArbre (n->fg, nbEspaces+10);
    }

}

void ABR::afficherParcoursInfixe () const
{
    afficherParcoursInfixeSousArbre(adracine,0);
}

void ABR::copieProfondeSousArbre (Noeud * n)
{
    if (n != nullptr)
    {
        insererElement(n->info);
        copieProfondeSousArbre(n->fg);
        copieProfondeSousArbre(n->fd);
    }
}

void ABR::operator = (const ABR & a)
{
    copieProfondeSousArbre(a.adracine);
}

void ABR::viderSousArbre (Noeud * n)
{
    if (n != nullptr)
    {
        viderSousArbre (n->fg);
        
        viderSousArbre (n->fd);
        delete n;
        nbElement--;
    }
}

void ABR::vider ()
{
    viderSousArbre(adracine);
}