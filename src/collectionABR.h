#ifndef _ABR_H
#define _ABR_H

#include "element.h"
#include "noeud.h"
#include <string>

using namespace std;


class ABR {

private:

    Noeud * adracine;
	int nbElement;


	/* 	Postcondition : Insère l'élément e dans le sous arbre de racine n
						Si e n'existe pas déjà dans l'arbre, alors un nouveau noeud contenant e est inséré. */
	void insererDansSousArbre (const Element & e, Noeud * & n);


	/* 	Postcondition : Recherche dans le sous arbre de racine n
                       	Si on a trouvé un noeud avec le e demandé, alors estPresent prend la valeur vrai. */
	void rechercherDansSousArbre (const Element & e,const Noeud * n, bool & estPresent) const;


	/* 	Postcondition : affichage infixe de tous les éléments de l'arbre (espacement entre les noeuds) */
	void afficherParcoursInfixeSousArbre (Noeud * n, int nbEspaces) const;

	/* 	Postcondition : Copie chaque noeud du sous-arbre de racine n dans this. */
	void copieProfondeSousArbre (Noeud * n);


	/*	Postcondition : Le sous arbre de racine n est vidé */
	void viderSousArbre (Noeud * n);



public:

	/*	Postcondition : l'arbre est créé vide */
    ABR ();


	/*	Postcondition : l'arbre est créé avec les valeurs de a */
    ABR (const ABR & a);
    

	/*	Postcondition : l'arbre est d'abord vidé, puis adracine est mis à nullptr */
    ~ABR ();


	/*	Postcondition : L'arbre est vide */
	void vider ();
    

	/*	Postcondition : l'arbre prend les noeuds de l'arbre a */
	void operator = (const ABR & a);
	

	/*	Postcondition : Si aucun noeud de l'arbre ne contient la valeur e, retourne faux.
                    	Si on a trouv� un noeud avec le e demandé, alors retourne vrai.*/
	bool rechercherElement (const Element & e) const;
   

	/*	Postcondition : Un nouveau noeud contenant e est inséré. */
	void insererElement (const Element & e);
    

	/*	Postcondition : affichage infixe de tous les �l�ments de l'arbre */
	void afficherParcoursInfixe () const;


};

#endif
