#ifndef _NOEUD_H
#define _NOEUD_H

#include "element.h"
#include <string>

using namespace std;

struct Noeud {
    Element info;
	string couleur;	// utile pour ARN, pas pour ABR
    Noeud * fg;
    Noeud * fd;
};


#endif
