all : bin/collection

bin/collection: obj/main.o obj/collectionARN.o obj/collectionABR.o obj/element.o
	g++ -g obj/main.o obj/collectionARN.o obj/collectionABR.o obj/element.o -o bin/collection

obj/main.o: src/main.cpp src/collectionARN.h src/collectionABR.h src/noeud.h src/element.h
	g++ -g -Wall -c src/main.cpp -o obj/main.o

obj/collectionARN.o: src/collectionARN.h src/collectionARN.cpp src/noeud.h src/element.h
	g++ -g -Wall -c src/collectionARN.cpp -o obj/collectionARN.o

obj/collectionABR.o: src/collectionABR.h src/collectionABR.cpp src/noeud.h src/element.h
	g++ -g -Wall -c src/collectionABR.cpp -o obj/collectionABR.o


obj/element.o: src/element.h src/element.cpp
	g++ -g -Wall -c src/element.cpp -o obj/element.o


clean:
	rm *.o

veryclean: clean
	rm *.out
